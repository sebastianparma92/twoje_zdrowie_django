# Aplikacja Twoje Zdrowie

## Opis

Aplikacja Twoje Zdrowie to projekt oparty na frameworku Django, który umożliwia użytkownikom prowadzenie ankiety dotyczącej ich stanu zdrowia. Aplikacja umożliwia użytkownikom zalogowanie się, wypełnienie ankiety zdrowotnej oraz przeglądanie swoich danych zdrowotnych.

## Wymagania

Python (zalecana wersja 3.6 lub nowsza)
Django (zainstalowane przy pomocy pip)
Instalacja

Sklonuj repozytorium:
```bash
git clone https://github.com/TWOJA-Nazwa-Uzytkownika/twoje-zdrowie-app.git
```
Przejdź do katalogu projektu:
```bash
cd twoje-zdrowie-app
```
Utwórz wirtualne środowisko Pythona (zalecane):
```bash
python -m venv venv
```
Aktywuj wirtualne środowisko:
Windows:
```bash
venv\Scripts\activate
```
macOS i Linux:
```bash
source venv/bin/activate
```
Zainstaluj wymagane biblioteki:
```bash
pip install -r requirements.txt
```
Wykonaj migracje bazy danych:
```bash
python manage.py migrate
```
Uruchom serwer deweloperski:
```bash
python manage.py runserver
```

Aplikacja powinna być dostępna pod adresem http://localhost:8000/ w przeglądarce internetowej.

## Użytkowanie

Aby zalogować się, przejdź do strony /auth/login/ i wprowadź swoje dane logowania.
Po zalogowaniu możesz wypełnić ankietę zdrowotną na stronie /health/.
Po wypełnieniu ankiety możesz przeglądać swoje dane zdrowotne na stronie /health/.
Aby wylogować się, przejdź do strony /auth/logout/.

## Kontrybucje

Jeśli chcesz przyczynić się do rozwoju tej aplikacji, proszę o przejrzenie CONTRIBUTING.md w celu uzyskania więcej informacji.

## Licencja

Ten projekt jest udostępniany na licencji MIT. Szczegóły można znaleźć w pliku LICENSE.

