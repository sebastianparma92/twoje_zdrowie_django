from django.apps import AppConfig


class TwojeZdrowieAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'twoje_zdrowie_app'
