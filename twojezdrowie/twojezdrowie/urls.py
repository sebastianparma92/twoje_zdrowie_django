from django.urls import path
from twoje_zdrowie_app import views
from django.contrib.auth.views import LogoutView


urlpatterns = [
    path('', views.home, name='home'),
    path('auth/login/', views.login_view, name='login'),
    path('auth/register/', views.register_view, name='register'),
    path('health/', views.health_survey_view, name='health-info'),
    path('thanks/', views.thanks_view, name='thanks'),
    path('logout/', views.logout_view, name='logout'),
    path('generate_pdf/', views.generate_pdf, name='generate_pdf'),
]
