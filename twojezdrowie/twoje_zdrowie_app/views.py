from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import HealthInfo, UserProfile
from django.http import FileResponse
from io import BytesIO
from reportlab.lib.pagesizes import letter
from reportlab.platypus import SimpleDocTemplate, Paragraph
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
import openai
import os

def home(request):
    if request.user.is_authenticated:
        return redirect('/health')
    return render(request, 'home.html')


def login_view(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('/health')
        else:
            # Obsługa błędnych danych logowania
            return render(request, 'login.html', {'error_message': 'Błędna nazwa użytkownika lub hasło.'})
    return render(request, 'login.html')

def logout_view(request):
    logout(request)
    return render(request, 'logout_success.html')


def register_view(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/auth/login')  # Przekierowanie po udanej rejestracji
    else:
        form = UserCreationForm()
    return render(request, 'register.html', {'form': form})


@login_required(login_url='home')
def health_survey_view(request):
    health_info, created = HealthInfo.objects.get_or_create(user=request.user)
    health_info.page = int(request.POST.get('page', 1))
    if request.method == 'POST':
        if health_info.page == 1:
            health_info.symptoms = request.POST.get('symptoms', '')
        elif health_info.page == 2:
            health_info.has_fever = request.POST.get('has_fever') == 'on'
        elif health_info.page == 3:
            health_info.chronic_illness = request.POST.get('chronic_illness', '')
        elif health_info.page == 4:
            health_info.weight = request.POST.get('weight', None)
        elif health_info.page == 5:
            health_info.height = request.POST.get('height', None)
        elif health_info.page == 6:
            health_info.age = request.POST.get('age', None)
        elif health_info.page == 7:
            health_info.has_family_illness = request.POST.get('has_family_illness') == 'on'
        elif health_info.page == 8:
            health_info.has_traveled = request.POST.get('has_traveled') == 'on'
        elif health_info.page == 9:
            health_info.has_contact = request.POST.get('has_contact') == 'on'
        elif health_info.page == 10:
            health_info.medications = request.POST.get('medications', '')
        elif health_info.page == 11:
            health_info.gender = request.POST.get('gender', '')
        elif health_info.page == 12:
            health_info.is_pregnant = request.POST.get('is_pregnant') == 'on'
        elif health_info.page == 13:
            health_info.address = request.POST.get('address', '')

        health_info.page = health_info.page + 1
        health_info.save()
        if health_info.page == 14:
            return redirect('thanks')

    return render(request, 'health_survey.html', {'health_info': health_info})

def generate_pdf(request):
    # Tworzenie pliku PDF w pamięci
    health_info, _ = HealthInfo.objects.get_or_create(user=request.user)
    user_profile, _ = UserProfile.objects.get_or_create(user=request.user)
    buffer = BytesIO()
    pdf = SimpleDocTemplate(buffer, pagesize=letter)
    pdfmetrics.registerFont(TTFont('ArialUnicode', 'twoje_zdrowie_app/static/fonts/Lato-Regular.ttf'))
    diagnose = response_from_gpt(request)['choices'][0]['message']['content']
    text = f"Imię: {user_profile.first_name}\n Nazwisko: {user_profile.last_name}\n Adres email: {user_profile.email}\n" \
           f"Płeć: {health_info.gender}\n Wiek: {health_info.age} lat\n " \
           f"Objawy: {health_info.symptoms} \n\n{diagnose}"

    # Tworzenie stylu dla tekstu
    styles = getSampleStyleSheet()
    style = styles["Normal"]
    style.fontName = 'ArialUnicode'
    paragraph = Paragraph(text, style=style)
    # Dodaj Paragraph do zawartości strony
    text_parts = text.split("\n")
    text_paragraphs = [Paragraph(part, style=style) for part in text_parts]

    # Dodaj Paragraphs do zawartości strony
    content = text_paragraphs
    # Budowanie dokumentu PDF
    pdf.build(content)

    # Przygotowanie pliku PDF do wysłania jako odpowiedź HTTP
    buffer.seek(0)
    response = FileResponse(buffer, as_attachment=True, filename='example.pdf')
    return response


def thanks_view(request):
    return render(request, 'thanks.html')


def response_from_gpt(request):
    health_info, _ = HealthInfo.objects.get_or_create(user=request.user)
    user_profile, _ = UserProfile.objects.get_or_create(user=request.user)
    openai.api_key = user_profile.gpt_token
    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=[
            {
                "role": "user",
                "content": ""
            },
            {
                "role": "assistant",
                "content": "Na podstawie moich objawów i danych krótko określ moją chorobę, udawaj prawdziwego lekarza."
                           " Podaj mi zalecenia do leczenia w domu. Napisz to w 5 zdaniach."
                           f"\n{health_info.gender}\n{health_info.age} lat\n{health_info.symptoms}"
            }
        ],
        temperature=1,
        max_tokens=300,
        top_p=1,
        frequency_penalty=0,
        presence_penalty=0
    )
    return response
