from django.db import models
from django.contrib.auth.models import User

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    phone_number = models.CharField(max_length=255)
    email = models.EmailField()
    medical_clinic_address = models.CharField(max_length=255)
    gpt_token = models.CharField(max_length=255)

    def __str__(self):
        return self.user.username

# Model informacji o zdrowiu
class HealthInfo(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    symptoms = models.TextField(blank=True)
    has_fever = models.BooleanField(default=False)
    chronic_illness = models.TextField(blank=True)
    weight = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    height = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    age = models.IntegerField(blank=True, null=True)
    has_family_illness = models.BooleanField(default=False)
    has_traveled = models.BooleanField(default=False)
    has_contact = models.BooleanField(default=False)
    medications = models.TextField(blank=True)
    gender_choices = [
        ('Kobieta', 'Kobieta'),
        ('Mężczyzna', 'Mężczyzna'),
        ('Nie chcę podawać', 'Nie chcę podawać'),
    ]
    gender = models.CharField(max_length=20, choices=gender_choices, blank=True)
    is_pregnant = models.BooleanField(default=False)
    address = models.TextField(blank=True)

    def __str__(self):
        return f"Health info for {self.user.username}"
